# EC2 Pipeline Demo



## Getting started

After forking this project you'll need to set up the following env variables accordingly:

``AWS_ACCESS_KEY_ID``

``AWS_DEFAULT_REGION``

``AWS_SECRET_ACCESS_KEY``

``TF_VAR_tag``

(defaults to "latest")

``TF_VAR_image``

(defaults to "nginxdemos/hello")


In the case you wish to deploy this locally, be sure to follow the gitlab instructions on how to authenticate against gitlab terraform managed service 

https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html

https://gitlab.com/<project-path>/-/terraform
