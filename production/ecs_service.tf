resource "aws_ecs_service" "ecs-demo" {
  name                               = local.name
  cluster                            = aws_ecs_cluster.ecs_demo.arn
  task_definition                    = aws_ecs_task_definition.ecs_demo.arn
  desired_count                      = 1
  deployment_maximum_percent         = 200
  deployment_minimum_healthy_percent = 100
  launch_type                        = "FARGATE"
  scheduling_strategy                = "REPLICA"

  network_configuration {
    security_groups  = compact(aws_security_group.ecs_demo_sg.*.id)
    subnets          = local.subnets
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.ecs_demo_tg.arn
    container_name   = local.name
    container_port   = 80
  }

  tags = local.tags
}
