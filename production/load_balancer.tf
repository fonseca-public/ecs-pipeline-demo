resource "aws_lb" "ecs_demo_lb" {
  name               = "alb"
  subnets            = local.subnets
  load_balancer_type = "application"
  security_groups    = [aws_security_group.ecs_demo_lb_sg.id]
  tags               = local.tags
}

resource "aws_lb_listener" "ecs_demo_lb_listener" {
  load_balancer_arn = aws_lb.ecs_demo_lb.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ecs_demo_tg.arn
  }
  tags = local.tags
}

resource "aws_lb_target_group" "ecs_demo_tg" {
  name        = local.name
  port        = 80
  protocol    = "HTTP"
  vpc_id      = local.vpc_id
  target_type = "ip"
  health_check {
    healthy_threshold   = "3"
    interval            = "90"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = "/"
    unhealthy_threshold = "2"
  }
  tags = local.tags
}

resource "aws_security_group" "ecs_demo_lb_sg" {
  name = "${local.name}-lb-sg"
  # the ALB can receive requests from anywhere on port 80
  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = local.tags
}
