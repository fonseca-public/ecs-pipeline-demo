data "aws_iam_policy_document" "ecs_demo_task_exec" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ecs_exec" {
  name               = local.name
  assume_role_policy = join("", data.aws_iam_policy_document.ecs_demo_task_exec.*.json)
  tags               = local.tags
}

data "aws_iam_policy_document" "ecs_demo_exec" {
  statement {
    effect    = "Allow"
    resources = ["*"]

    actions = [
      "ssm:GetParameters",
      "ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage",
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
  }
}

resource "aws_iam_role_policy" "ecs_exec" {
  name   = local.name
  policy = join("", data.aws_iam_policy_document.ecs_demo_exec.*.json)
  role   = join("", aws_iam_role.ecs_exec.*.id)
}
