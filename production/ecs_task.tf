resource "aws_cloudwatch_log_group" "ecs_demo_log_group" {
  name_prefix       = "${local.name}-"
  retention_in_days = 1
  tags              = local.tags
}

resource "aws_ecs_task_definition" "ecs_demo" {
  family                   = local.name # must be unique
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture        = "X86_64"
  }
  execution_role_arn    = join("", aws_iam_role.ecs_exec.*.arn)
  container_definitions = <<TASK_DEFINITION
[
  {
    "name": "${local.name}",
    "image": "${var.image}:${var.tag}",
    "cpu": 128,
    "memory": 256,
    "essential": true,
    "portMappings": [
        {
          "hostPort": 80,
          "protocol": "tcp",
          "containerPort": 80
        }
      ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-region": "${local.region}",
        "awslogs-group": "${aws_cloudwatch_log_group.ecs_demo_log_group.name}",
        "awslogs-stream-prefix": "ec2"
      }
    }
  }
]
TASK_DEFINITION
  tags                  = local.tags
}
