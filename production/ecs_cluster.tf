resource "aws_ecs_cluster" "ecs_demo" {
  name = local.name
  configuration {
    execute_command_configuration {
      logging = "OVERRIDE"
      log_configuration {
        cloud_watch_log_group_name = "/aws/ecs/aws-ec2"
      }
    }
  }
  setting {
    name  = "containerInsights"
    value = "enabled"
  }
  tags = local.tags
}
