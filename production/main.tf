terraform {
  backend "http" {
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 2
  }
  required_version = ">= 1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.6"
    }
  }
}

provider "aws" {
  region = local.region
}

locals {
  name   = "ecs-demo"
  region = "eu-west-3"
  tags = {
    app = "ecs-demo"
  }
  vpc_id  = data.aws_vpc.default.id
  subnets = data.aws_subnets.default.ids
}

variable "tag" {
  type    = string
  default = "latest"
}

variable "image" {
  type    = string
  default = "nginxdemos/hello"
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnets" "default" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }
}