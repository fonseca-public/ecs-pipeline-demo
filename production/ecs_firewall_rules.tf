## Security Groups
resource "aws_security_group" "ecs_demo_sg" {
  vpc_id = local.vpc_id
  name   = local.name
  lifecycle {
    create_before_destroy = true
  }
  tags = local.tags
}
resource "aws_security_group_rule" "allow_all_egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = join("", aws_security_group.ecs_demo_sg.*.id)
}
resource "aws_security_group_rule" "allow_icmp_ingress" {
  type              = "ingress"
  from_port         = 8
  to_port           = 0
  protocol          = "icmp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = join("", aws_security_group.ecs_demo_sg.*.id)
}
resource "aws_security_group_rule" "allow_http_ingress" {
  type                     = "ingress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.ecs_demo_lb_sg.id # only from the ALB
  security_group_id        = join("", aws_security_group.ecs_demo_sg.*.id)
}
